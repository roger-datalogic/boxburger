<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    /*
     * {
      "id": 788303233,
      "nickname": "TT656746",
      "password": "qatest12",
      "site_status": "active",
      "email": "test_user_82784462@testuser.com"
    }
    {
      "id": 788305467,
      "nickname": "TETE8788378",
      "password": "qatest8816",
      "site_status": "active",
      "email": "test_user_31101561@testuser.com"
    }
     * */

    'mercadopago' => [
        'key' => env('MP_KEY'),
        'token' => env('MP_TOKEN')
    ]

];
