const mix = require('laravel-mix');
const fs = require('fs');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/web.js', 'public/js')
    .copyDirectory('resources/images', 'public/images')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-nested'),
        require("tailwindcss"),
    ])
    .postCss('resources/css/web.css', 'public/css', [
        require('postcss-nested'),
        require("tailwindcss"),
    ])
    .vue();

//mix.browserSync(process.env.APP_URL);
