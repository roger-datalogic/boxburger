const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                'gray': colors.trueGray,
            },
            backgroundImage: theme => ({
                'hero': "url('/storage/pictures/home-custom-mobile.jpg')",
                'hero-md': "url('/storage/pictures/home-custom-desktop.jpg')",
                'footer': "url('../images/bg-footer.jpg')",
                'pattern': "url('../images/bg.png')",
                'pattern-dark': "url('../images/bg-dark.png')"
            }),
            fontFamily: {
                sans: ['Inter var', ...defaultTheme.fontFamily.sans],
            },
        },
    },
    variants: {
        extend: {
            ringColor: ['hover', 'active'],
            backgroundColor: ['disabled'],
            textColor: ['disabled'],
            cursor: ['disabled']
        },
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
