<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MercadoPago\Payment;
use MercadoPago\SDK;
use Illuminate\Support\Facades\Log;

class PublicController extends Controller
{
    function index(){
        $categories = Category::orderBy('position')->get();
        return view('welcome', [
            'categories' => $categories
        ]);
    }

    function apiProducts(Category $category){
        return Product::orderBy('price')->where('category_id', $category->id)->get();
    }

    public static function confirmOrder($order){
        $firstName = explode(" ", $order->client->name)[0];

        Carbon::setLocale('es');
        $fullDate = Carbon::createFromFormat("Y-m-d H:i:s", $order->created_at)->setTimezone('America/Bogota');
        $mes = getMonth($fullDate->format("m"));
        $date = $mes . " " . $fullDate->format('j') . " de " . $fullDate->format('Y,');
        $time = " " . $fullDate->format("h:i A");

        $pago = "En Línea.";
        if($order->preference == 0){
            $pago = "Con Datáfono.";
        }

        $template = "buy-success";
        $entrega = "Domicilio.";
        if($order->shipping == 0){
            $template = "buy-success-1";
            $entrega = "Recoger en BoxBurger Cota.";
        }

        // Send email
        $client = new Client();
        $headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "X-Postmark-Server-Token" => "c814497a-a76c-4b0c-b555-6e542e0d9cd1"
        ];
        $body = '{"From":"Box Burger <hola@boxburger.com.co>","Bcc":"ngeinversiones@gmail.com", "To":"' . $order->client->email . '","ReplyTo":"hola@boxburger.com.co","TemplateAlias":"' . $template . '","TemplateModel":{"shipping": "$' . $order->shipping . '.", "address": "' . $order->client->address . '", "pago": "' . $pago . '", "entrega": "'. $entrega .'", "name":"' . $firstName. '","receipt_id":"' . $order->id . '","date":"' . $date. '","time":"' . $time . '","total":"$' . $order->total . '."}}';
        $request = new \GuzzleHttp\Psr7\Request('POST', "https://api.postmarkapp.com/email/withTemplate", $headers, $body);
        $client->send($request);
    }

    public function confirmSuccess(Request $request){
        Log::debug($request->all());

        if($request->has('resource') && $request->get('topic') === "merchant_order") {
            $client = new Client();
            $headers = [
                "Content-Type" => "application/json",
                "Accept" => "application/json",
                "Authorization" => "Bearer " . env('MP_TOKEN')
            ];
            $request = new \GuzzleHttp\Psr7\Request('GET', "https://api.mercadopago.com/merchant_orders/" . $request->get('id'), $headers);
            $response = $client->send($request);
            $responseJSON = json_decode($response->getBody(), true);

            $external_reference = $responseJSON["external_reference"];
            $paymentId = $responseJSON["payments"][0]["id"];
            $order = \App\Models\Order::find($external_reference);

            SDK::setAccessToken(config('services.mercadopago.token'));

            $order->status = $responseJSON["status"];
            $order->payment_id = $paymentId;
            $order->save();

            if ($responseJSON["status"] === "closed" && $order->status !== "approved") {
                $order->status = "approved";
                $order->save();
                self::confirmOrder($order);
                return response('OK', 200);
            }
        }

        return response('OK', 200);

    }
}
