<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class OthersController extends Controller
{
    function index(){
        return view('admin.others.index');
    }

    function create(){
        return view('admin.categories.create');
    }

    function save(Request $request){
        if($request->has('picture')){
            //File::delete("home-custom-desktop.jpg");
            $path = $request->file('picture')->storeAs('pictures', 'home-custom-desktop.jpg', ['disk' => 'public']);
        }

        if($request->has('picture2')){
            //File::delete("home-custom-mobile.jpg");
            $path2 = $request->file('picture2')->storeAs('pictures', 'home-custom-mobile.jpg', ['disk' => 'public']);
        }

        return Redirect::route('admin.others')->header('Cache-Control', 'no-store, no-cache, must-revalidate');
    }

    function edit(Category $category){
        return view('admin.categories.edit', [
            'category' => $category
        ]);
    }

    function update(Request $request, Category $category){

    }
}
