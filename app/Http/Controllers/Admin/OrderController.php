<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    function index(){
        $items = Order::orderBy('id', 'DESC');

        /*if(\request()->has('category_id') && !empty(\request()->get('category_id'))){
            $selectedCategory = \request()->get('category_id');
            $items = $items->where('category_id', $selectedCategory);
        }*/

        return view('admin.orders.index', [
            'items' => $items->paginate(15)
        ]);
    }

    function details(Order $order){
        Carbon::setLocale('es');
        $fullDate = Carbon::createFromFormat("Y-m-d H:i:s", $order->created_at)->setTimezone('America/Bogota');
        $mes = getMonth($fullDate->format("m"));
        $date = $mes . " " . $fullDate->format('j') . " de " . $fullDate->format('Y,');
        $time = " " . $fullDate->format("h:i A");

        $pago = "En Línea.";
        if($order->preference == 0){
            $pago = "Con Datáfono.";
        }

        $entrega = "Domicilio.";
        if($order->shipping == 0){
            $entrega = "Recogida.";
        }

        return view('admin.orders.details', [
            'order' => $order,
            'entrega' => $entrega,
            'pago' => $pago,
            'fecha' => $date,
            'hora' => $time
        ]);
    }

}
