<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    function index(){
        return view('admin.categories.index', [
            'items' => Category::orderBy('position')->get()
        ]);
    }

    function create(){
        return view('admin.categories.create');
    }

    function save(Request $request){
        Category::create([
            'name' => $request->get('name'),
            'position' => $request->get('position'),
        ]);

        return Redirect::route('admin.categories');
    }

    function edit(Category $category){
        return view('admin.categories.edit', [
            'category' => $category
        ]);
    }

    function update(Request $request, Category $category){
        $category->update([
            'name' => $request->get('name'),
            'position' => $request->get('position'),
        ]);

        return Redirect::route('admin.categories');
    }
}
