<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ingredient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;

class IngredientController extends Controller
{
    function index(){
        return view('admin.ingredients.index', [
            'items' => Ingredient::orderBy('name')->get()
        ]);
    }

    function create(){
        return view('admin.ingredients.create');
    }

    function save(Request $request){
        Ingredient::create(['name' => $request->get('name')]);

        return Redirect::route('admin.ingredients');
    }
}
