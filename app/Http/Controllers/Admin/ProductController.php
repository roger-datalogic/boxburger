<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    function index(){
        $categories = Category::orderBy('name')->get();
        $selectedCategory = null;

        $items = Product::orderBy('name');

        if(\request()->has('category_id') && !empty(\request()->get('category_id'))){
            $selectedCategory = \request()->get('category_id');
            $items = $items->where('category_id', $selectedCategory);
        }

        return view('admin.products.index', [
            'items' => $items->get(),
            'categories' => $categories,
            'selectedCategory' => $selectedCategory
        ]);
    }

    function create(){
        $ingredients = Ingredient::orderBy('name')->get();
        $categories = Category::orderBy('name')->get();
        return view('admin.products.create', [
            'ingredients' => $ingredients,
            'categories' => $categories,
        ]);
    }

    function save(Request $request){
        $path = $request->file('picture')->store('pictures', ['disk' => 'public']);

        $product = new Product();
        $product->name = $request->get('name');
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->category_id = $request->get('category_id');
        $product->picture = $path;
        $product->save();

        if($request->has('ingredients') && !empty($request->get('ingredients'))){
            $ingredients = explode('|', $request->get('ingredients'));
            $product->ingredients()->sync($ingredients);
        }

        if($request->has('additions') && !empty($request->get('additions'))){
            $additions = explode('|', $request->get('additions'));
            $product->additions()->detach();

            foreach($additions as $addition){
                $additionArray = explode("-", $addition);
                $product->additions()->attach([$additionArray[0] => ['price' => $additionArray[1]]]);
            }
        }

        return \redirect()->route('admin.products');
    }

    function edit(Product $product){
        $ingredients = Ingredient::orderBy('name')->get();
        $categories = Category::orderBy('name')->get();
        return view('admin.products.edit', [
            'product' => $product,
            'ingredients' => $ingredients,
            'categories' => $categories,
        ]);
    }

    function update(Request $request, Product $product){
        $ingredients = explode('|', $request->get('ingredients'));

        $product->name = $request->get('name');
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->category_id = $request->get('category_id');

        if($request->hasFile('picture')){
            $path = $request->file('picture')->store('pictures', ['disk' => 'public']);
            $product->picture = $path;
        }

        $product->save();

        if($request->has('ingredients') && !empty($request->get('ingredients'))){
            $ingredients = explode('|', $request->get('ingredients'));
            $product->ingredients()->sync($ingredients);
        }

        if($request->has('additions') && !empty($request->get('additions'))){
            $additions = explode('|', $request->get('additions'));
            $product->additions()->detach();

            foreach($additions as $addition){
                $additionArray = explode("-", $addition);
                $product->additions()->attach([$additionArray[0] => ['price' => $additionArray[1]]]);
            }
        }

        return \redirect()->route('admin.products');
    }
}
