<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    protected $with = ['ingredients', 'additions'];

    function ingredients(): BelongsToMany
    {
        return $this->belongsToMany('\App\Models\Ingredient')->withPivot('can_be_personalized');
    }

    function additions(): BelongsToMany
    {
        return $this->belongsToMany('\App\Models\Ingredient', 'additions')->withPivot('price');
    }

    function category(): BelongsTo
    {
        return $this->belongsTo('\App\Models\Category');
    }
}
