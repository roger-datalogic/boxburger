<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class troja extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'troja:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update troja songs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        //Get last
        $last = DB::table("troja_songs")->orderBy("id", "DESC")->first();

        $client = new Client();
        $headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Authorization" => "HMAC radiocolombia_web:Nk-hJwE6nGk1HxT_7vxu92fHWD3bij-hPfrkyOO4TnI"
        ];
        $request = new Request('GET', "https://metadata-api.mytuner.mobi/api/v1/metadata-api/web/metadata?app_codename=radiocolombia_web&radio_id=452816&time=1633103071948", $headers);
        $response = $client->send($request);
        $body = json_decode($response->getBody(), true);
        $played_at = Carbon::now()->timezone("America/Bogota");
        $name = $body["radio_metadata"]["metadata"];
        $artwork = $body["radio_metadata"]["artwork_url_small"];

        if(!$last && trim($name)){
            DB::table("troja_songs")->insert([
                "name" => $name,
                "artwork" => $artwork,
                "played_at" => $played_at->format("Y-m-d H:i:s")
            ]);
        }else if($last && trim($name) != "" && trim($last->name) != trim($name)){
            DB::table("troja_songs")->insert([
               "name" => $name,
                "artwork" => $artwork,
                "played_at" => $played_at->format("Y-m-d H:i:s")
            ]);
        }

    }
}
