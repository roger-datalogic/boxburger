<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\IngredientController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\OthersController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\PublicController;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function(){
    return view('soon');
});*/

function getMonth($index){
    $months = [];
    $months[0] = "Enero";
    $months[1] = "Febrero";
    $months[2] = "Marzo";
    $months[3] = "Abril";
    $months[4] = "Mayo";
    $months[5] = "Junio";
    $months[6] = "Julio";
    $months[7] = "Agosto";
    $months[8] = "Septiembre";
    $months[9] = "Octubre";
    $months[10] = "Noviembre";
    $months[11] = "Diciembre";

    return $months[$index-1];
}

Route::post('/ipcap/contact', static function(\Illuminate\Http\Request $request){
    Http::withHeaders([
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'X-Postmark-Server-Token' => '054288c0-346f-4387-ba98-ce061d062940'
    ])->post('https://api.postmarkapp.com/email', [
        "From" => "IP Capital Partners <ir@ipcappartners.com>",
        "To" => "ir@ipcappartners.com",
        "Bcc" => "asmith@protechmsp.com",
        "Subject" => "Contact Form from IPCP website",
        "TextBody" => "...",
        "HtmlBody" => $request->get('HtmlBody'),
        "MessageStream" => 'outbound',
    ]);
});

Route::get('/troja/songs', function(){
    return DB::table("troja_songs")->orderBy("id", "DESC")->take(250)->get();
});

Route::get('/', [PublicController::class, 'index']);

Route::get('/time', function(){
    return Carbon::now('America/Bogota')->format("H");
});

Route::post('/mp/ipn', [PublicController::class, 'confirmSuccess']);

Route::get('/pago/success', function (\Illuminate\Http\Request $request){
    $order = null;

    /*if($request->has("order")){
        $order = \App\Models\Order::where('id', $request->get("order"))->first();
    }else{
        if($request->has("payment_id")){
            $external_reference = $request->get("external_reference");
            $paymentId = $request->get("payment_id");
            $order = \App\Models\Order::find($external_reference);
            $order->payment_id = $paymentId;
            $order->save();
        }else{
            $order = \App\Models\Order::orderBy('id', 'desc')->first();
        }
    }


    $order->status = "approved";
    $order->save();
    PublicController::confirmOrder($order);*/

    return view('success');
});

Route::get('/pago/failure', function (\Illuminate\Http\Request $request){
    if($request->has("payment_id") && $request->has("preference_id")){
        $preference = $request->get("preference_id");
        $paymentId = $request->get("payment_id");
        $order = \App\Models\Order::where('preference', $preference)->first();
        $order->payment_id = $paymentId;
    }else{
        $order = \App\Models\Order::orderBy('id', 'desc')->first();
    }

    $order->status = "fail";
    $order->save();

    return view('fail');
});

Route::get('/pago/pending', function (\Illuminate\Http\Request $request){
    if($request->has("payment_id")){
        $preference = $request->get("preference_id");
        $paymentId = $request->get("payment_id");
        $order = \App\Models\Order::where('preference', $preference)->first();
        $order->payment_id = $paymentId;

    }else{
        $order = \App\Models\Order::orderBy('id', 'desc')->first();
    }

    $order->status = "pending";
    $order->save();

    return view('pending');
});


Route::prefix('api')->group(function(){
    Route::get('/products/{category}', [PublicController::class, 'apiProducts']);

    Route::post('/save-manual-order', function(\Illuminate\Http\Request $request){
        $total = 0;

        $cart = json_decode($request->get('cart'));
        $shipping = json_decode($request->get('shipping'));
        $payment = json_decode($request->get('payment'));

        $client = new \App\Models\Client();
        $order = new \App\Models\Order();

        $client->name = $shipping->name;
        $client->email = $shipping->email;
        $client->address = $shipping->address;
        $client->phone = $shipping->phone;
        $client->save();

        $order->client_id = $client->id;
        $order->shipping = 3000;

        $order->comments = "";

        if(!empty($shipping->comments)){
            $order->comments = $shipping->comments;
        }

        $order->save();

        $products = [];

        foreach($cart as $product){
            $total += $product->qty * $product->price;

            $orderDetail = new \App\Models\OrderDetail();
            $orderDetail->order_id = $order->id;
            $orderDetail->product = $product->name;
            $orderDetail->qty = $product->qty;
            $orderDetail->total =  $product->qty * $product->price;

            $notes = "";
            $coma = "";
            foreach($product->ingredients as $ingredient){
                if(!$ingredient->include){
                    $notes .= $coma . "Sin " . $ingredient->name;
                    $coma = ", ";
                }
            }

            $orderDetail->notes =  $notes;
            $orderDetail->save();
        }

        $order->total = $total;
        $order->preference = 0;
        $order->save();

        return $order->id;
    });

    Route::post('/mp/preference', function(\Illuminate\Http\Request $request){
        $total = 0;

        MercadoPago\SDK::setAccessToken(config('services.mercadopago.token'));
        $preference = new MercadoPago\Preference();
        $preference->back_urls = array(
            "success" => "https://boxburger.com.co/pago/success",
            "failure" => "https://boxburger.com.co/pago/failure",
            "pending" => "https://boxburger.com.co/pago/pending"
        );
        $preference->auto_return = "approved";
        $preference->notification_url = "https://boxburger.com.co/mp/ipn";


        $cart = json_decode($request->get('cart'));
        $shipping = json_decode($request->get('shipping'));
        $payment = json_decode($request->get('payment'));

        $client = new \App\Models\Client();
        $order = new \App\Models\Order();

        $client->name = $shipping->name;
        $client->email = $shipping->email;
        $client->address = $shipping->address;
        $client->phone = $shipping->phone;
        $client->save();

        $order->client_id = $client->id;
        $order->comments = "";

        if(!empty($shipping->comments)){
            $order->comments = $shipping->comments;
        }
        $order->shipping = 3000;
        if($shipping->method == "recogida"){
            $order->shipping = 0;
        }else{
            $shipment = new stdClass();
            $shipment->mode = "not_specified";
            $shipment->cost = 3000;

            $preference->shipments = $shipment;
        }
        $order->save();

        $products = [];

        foreach($cart as $product){
            $item = new MercadoPago\Item();
            $item->title = $product->name;
            $item->quantity = $product->qty;
            $item->unit_price = $product->price;
            $products[] = $item;

            $total += $product->qty * $product->price;

            $orderDetail = new \App\Models\OrderDetail();
            $orderDetail->order_id = $order->id;
            $orderDetail->product = $product->name;
            $orderDetail->qty = $product->qty;
            $orderDetail->total =  $product->qty * $product->price;

            $notes = "";
            $coma = "";
            foreach($product->ingredients as $ingredient){
                if(!$ingredient->include){
                    $notes .= $coma . "Sin " . $ingredient->name;
                    $coma = ", ";
                }
            }

            $orderDetail->notes =  $notes;
            $orderDetail->save();
        }

        $payer = new MercadoPago\Payer();
        $payer->name = $shipping->name;
        $payer->email = $shipping->email;
        $payer->date_created = \Carbon\Carbon::now();
        $payer->phone = array(
            "area_code" => "1",
            "number" => $shipping->phone
        );

        $payer->address = array(
            "street_name" => $shipping->address
        );

        $preference->payer = $payer;

        $preference->items = $products;

        $order->total = $total;
        $order->save();

        $preference->external_reference = $order->id;
        $preference->save();

        $order->preference = $preference->id;
        $order->save();

        return $preference->id;
    });

});


Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return view('admin.login');
    });

    Route::get('dashboard', function () {
        return view('admin.dashboard');
    })->name('admin.dashboard');

    Route::post('/', [UserController::class, 'login'])->name('admin.login');

    Route::get('/categories', [CategoryController::class, 'index'])->name('admin.categories');
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('admin.categories.create');
    Route::post('/categories', [CategoryController::class, 'save'])->name('admin.categories.save');
    Route::get('/categories/{category}', [CategoryController::class, 'edit'])->name('admin.categories.edit');
    Route::post('/categories/{category}', [CategoryController::class, 'update'])->name('admin.categories.update');

    Route::get('/ingredients', [IngredientController::class, 'index'])->name('admin.ingredients');
    Route::get('/ingredients/create', [IngredientController::class, 'create'])->name('admin.ingredients.create');
    Route::post('/ingredients', [IngredientController::class, 'save'])->name('admin.ingredients.save');

    Route::get('/products', [ProductController::class, 'index'])->name('admin.products');
    Route::get('/products/create', [ProductController::class, 'create'])->name('admin.products.create');
    Route::post('/products', [ProductController::class, 'save'])->name('admin.products.save');
    Route::get('/products/{product}/edit', [ProductController::class, 'edit'])->name('admin.products.edit');
    Route::post('/products/{product}', [ProductController::class, 'update'])->name('admin.products.update');

    Route::get('/orders', [OrderController::class, 'index'])->name('admin.orders');
    Route::get('/orders/{order}', [OrderController::class, 'details'])->name('admin.orders.details');

    Route::get('/others', [OthersController::class, 'index'])->name('admin.others');
    Route::post('/others', [OthersController::class, 'save'])->name('admin.others.save');

});
