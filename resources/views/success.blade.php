<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Estás aquí para ser feliz - Box Burger - Comidas rápidas en Cota, Cundinamarcas</title>
    <link rel="stylesheet" href="{{ mix('css/web.css') }}">
</head>

<body class="bg-pattern">

<header class="bg-white p-4 shadow-lg">
    <div class="flex items-center mr-4">
        <a href="/">
            <div class="w-24 h-auto md:w-32"> <!-- logo Home svg -->
                <img src="/images/main_images/header/logo-box-burger.png" alt="">
            </div>
        </a>
    </div>
</header>

<div class="max-w-3xl md:grid grid-cols-5 gap-10 mx-auto p-10">
    <div class="col-span-2 hidden md:block">
        <img class="rounded-lg" src="/images/payment-burger.jpg" alt="">
    </div>

    <div class="col-span-3">
        <h1 class="text-xl md:text-2xl text-green-500 font-semibold mb-4">Gracias por tu pedido.</h1>
        <p class="mb-5 font-medium text-lg">Para BoxBurger fue un placer haberte atendido y esperamos que lo disfrutes.</p>
        <p class="mb-5">En unos minutos recibirás un correo electrónico con los detalles de tu ordén.</p>

        <a href="/" class="text-2xl block text-center uppercase bg-black hover:bg-gray-400 rounded text-white transition-all duration-500 text-base px-4 py-2 font-semibold">
            <span>VOLVER AL INICIO</span>
        </a>
    </div>
</div>

<script>
    localStorage.removeItem("boxburgercart")
</script>

</body>
</html>
