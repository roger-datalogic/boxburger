<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Estás aquí para ser feliz - Box Burger - Comidas rápidas en Cota, Cundinamarcas</title>
    <link rel="stylesheet" href="{{ mix('css/web.css') }}">

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '131596789183611');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=131596789183611&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body class="relative bg-pattern">
<div id="location" class="z-50 visible hidden fixed top-0 left-0 shadow-lg w-full min-h-screen bg-black bg-opacity-80 flex justify-center items-center">
    <div class="rounded-xl bg-white p-6 m-6 relative">
        <svg  onclick="closeModal()" xmlns="http://www.w3.org/2000/svg" class="absolute top-6 right-6 h-10 w-10 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
        </svg>

        <div class="mb-6">
            <h1 class="font-semibold text-4xl text-center mb-6">¡Te estamos esperando!</h1>
            <div class="flex space-x-4 justify-center items-center mb-2">
                <img src="/images/waze.png" alt="Buscanos en Waze como BURGER BOX CO">
                <img src="/images/map.png" alt="Buscanos en Google Maps como BURGER BOX CO">
            </div>
            <p class="text-center font-semibold text-lg">Búscanos en Waze ó Google Maps.</p>

            <p class="text-center">
                Estamos ubicados en Cota, Cundinamarca, en la Calle 10 No. 05-03.
            </p>
        </div>
        <div>
            <img class="w-full block" src="{{ asset('images/location.jpg') }}">
        </div>
    </div>
</div>

<div id="blocker" class="z-50 visible hidden fixed top-0 left-0 w-full min-h-screen bg-white bg-opacity-80 flex justify-center items-center">
    <img class="mx-auto block" src="/images/burger.gif">
</div>

<div id="app">
    <home :categories="{{ $categories }}"></home>
    <foot></foot>
</div>

<script src="{{ mix('js/web.js') }}"></script>

<script>
    function closeModal(){
        document.getElementById("location").classList.remove("visible")
        document.getElementById("location").classList.add("hidden")
    }
</script>
</body>
</html>
