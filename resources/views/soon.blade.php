<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Box Burger | La Mejor Hamburguesa en Cota | Pide en línea</title>
    <link rel="stylesheet" href="{{ asset('css/web.css') }}">
</head>

<body class="bg-gray-800">
<div class="min-h-screen flex justify-center items-center">
    <div>
        <div class="w-48">
            <img class="mx-auto" src="{{ asset('images/logo-box-burger-inverted.png') }}" alt="">
        </div>          
        <p class="text-gray-400 text-center mt-4">Próximamente</p>
    </div>
</div>
</body>
</html>
