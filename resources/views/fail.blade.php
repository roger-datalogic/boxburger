<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Estás aquí para ser feliz - Box Burger - Comidas rápidas en Cota, Cundinamarcas</title>
    <link rel="stylesheet" href="{{ mix('css/web.css') }}">
</head>

<body class="bg-pattern">

<header class="bg-white p-4 shadow-lg">
    <div class="flex items-center mr-4">
        <a href="/">
            <div class="w-24 h-auto md:w-32"> <!-- logo Home svg -->
                <img src="/images/main_images/header/logo-box-burger.png" alt="">
            </div>
        </a>
    </div>
</header>

<div class="max-w-3xl md:grid grid-cols-5 gap-10 mx-auto p-10">
    <div class="col-span-2 hidden md:block">
        <img class="rounded-lg" src="/images/payment-burger.jpg" alt="">
    </div>

    <div class="col-span-3">
        <h1 class="text-xl md:text-2xl text-red-500 font-semibold">Tu pago fue rechazado.</h1>
        <p class="mb-5">Ocurrió un problema al procesar tu pago, pero no te preocupes, tenemos opciones para ti:</p>
        <a href="https://wa.me/573166342196" target="_blank" class="block rounded-lg text-center p-4 text-xl font-semibold border-2 border-green-200 bg-green-100">
            <div class="text-center mb-3">
                <img class="mx-auto" src="{{ asset('images/whatsapp.png') }}" alt="">
            </div>
            <div class="text-center leading-6">
                Haz click para hacer tu pedido a través de WhatsApp.
            </div>
        </a>
    </div>
</div>

</body>
</html>
