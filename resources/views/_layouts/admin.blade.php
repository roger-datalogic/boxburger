<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <title>Box Burger - Panel de Administración</title>
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
    <style>
        span a{
            color: #b1b1b1 !important;
        }
    </style>
</head>
<body class="antialiased">

<div class="flex min-h-screen">
    <div class="bg-gray-800 text-white p-10">

        <div class="mb-10">
            <div class="mb-6">
                <img class="h-16 w-auto" src="{{ asset('images/logo-box-burger-inverted.png') }}" alt="Logo Box Burger">
            </div>

            <ul class="text-xl space-y-2">
                <li><a href="{{ route('admin.categories') }}">Categorías</a></li>
                <li><a href="{{ route('admin.ingredients') }}">Ingredientes</a></li>
                <li><a href="{{ route('admin.products') }}">Productos</a></li>
                <li><a href="{{ route('admin.orders') }}">Pedidos</a></li>
                <li><a href="{{ route('admin.others') }}">Imagen Home</a></li>
            </ul>
        </div>


        <div class="border-t border-gray-700 pt-10">
            <div class="flex items-center mb-3">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 mr-2" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z" clip-rule="evenodd" />
                </svg>

                <div>
                    <div>{{ Auth::user()->name }}</div>
                    <div>{{ Auth::user()->email }}</div>
                </div>
            </div>

            <a href="#" class="flex block items-center justify-center px-4 py-2 border border-gray-300 shadow-sm text-base font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <svg class="w-5 h-5 mr-2" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                </svg>
                <span>Salir</span>
            </a>
        </div>

    </div>

    <div id="app" class="bg-gray-200 flex-1 p-10">
        @yield('content')
    </div>
</div>


<script src="{{ mix('js/app.js') }}"></script>

</body>
</html>
