<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detalle de Orden {{ $order->id }}</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <style>
        @media print {

            @page {
                size: 4in 11in;
                margin: 5%;
            }

            #main {
                width: 100%;
            }

            td, th{
                padding: 3px;
            }

        }

        td, th{
            padding: 3px;
        }
    </style>
</head>
<body>

<?php
$status = "";
if($order->status == "approved"){
    $status = "Aprobado";
}elseif($order->status == "pending"){
    $status = "Pendiente";
}else {
    $status = "Fallido";
}
?>

<!-- "shipping": "$' . $order->shipping . '.", "address": "' . $order->client->address . '", "pago": "' . $pago . '", "entrega": "'. $entrega .'", "name":"' . $firstName. '","receipt_id":"' . $order->id . '","date":"' . $date. '","time":"' . $time . '","total":"$' . $order->total .-->

<div id="main" class="p-10">
    <div class="text-2xl">Pedido No. {{ $order->id }}</div>
    <div>Tipo de pago: {{ $pago }}</div>
    <div>Tipo de entrega: {{ $entrega }}</div>
    <div>Total: ${{ number_format($order->total, 0, ".", ".") }} @if($order->shipping > 0) + ${{ number_format($order->shipping, 0, ".", ".") }} @endif</div>
    <div>{{ $fecha }} {{ $hora }}</div>

    @if($order->payment_id)
        <div>Estado: {{ $status }}</div>
    @endif

    <div class="mt-5 text-xs">Cliente</div>
    <div>{{ $order->client->name }}</div>
    <div>{{ $order->client->email }} / {{ $order->client->phone }}</div>
    <div>{{ $order->client->address }}</div>

    @if($order->comments)
    <div>{{ $order->comments }}</div>
    @endif

    <div class="mt-5 text-xs">Pedido</div>
    <table class="border-collapse">
        <thead>
            <tr>
                <th class="text-left">Producto</th>
                <th class="text-right">Cant.</th>
                <th class="text-right">Valor</th>
            </tr>
        </thead>

        <tbody>
        @foreach($order->items as $item)
            <tr>
                <td class="text-left">{{ $item->product }}</td>
                <td class="text-right">{{ $item->qty }}</td>
                <td class="text-right">${{ number_format($item->total, 0, ".", ".") }}</td>
            </tr>
            <tr>
                <td colspan="3" class="text-sm pl-10">{{ $item->notes }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script>
    print()
</script>

</body>
</html>
