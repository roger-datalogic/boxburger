@extends('_layouts.admin')

@section('content')

    <div class="flex items-center justify-between mb-5">
        <h1 class="text-2xl font-semibold">Pedidos</h1>
    </div>

    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                        <tr>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                ID
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Cliente
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider text-right">
                                Total
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Tipo
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Status
                            </th>
                            <th scope="col" class="relative px-6 py-3">
                                &nbsp;
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                        <tr @if($loop->index % 2 == 0) class="bg-white" @else class="bg-gray-50" @endif>
                            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {{ $item->id }}
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                <div>{{ $item->client->name }}</div>
                                <div class="text-xs">{{ $item->client->email }} - {{ $item->client->phone }}</div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900 text-right">
                                <div>${{ number_format($item->total, 0, ".", ".") }}</div>

                                @if($item->shipping > 0)
                                <div class="text-xs">+ ${{ number_format($item->shipping, 0, ".", ".") }}</div>
                                @endif
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                @if($item->preference && $item->preference !== 0)
                                    Pago en Línea
                                @else
                                    Datáfono
                                @endif

                                /

                                @if($item->shipping > 0)
                                    Domicilio
                                @else
                                    Recogida
                                @endif
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                <div>
                                    @if($item->preference && $item->preference !== 0)
                                        @if($item->status == "approved")
                                            <div class="flex items-center space-x-1 text-green-700 font-bold">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                                </svg>
                                                <div>Aprobado</div>
                                            </div>
                                        @elseif($item->status == "pending")
                                            <div class="text-yellow-600">Pendiente</div>
                                        @else
                                            <div class="text-red-600">Fallido</div>
                                        @endif

                                    @else
                                        <div class="flex items-center space-x-1 text-green-700 font-bold">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                            </svg>
                                            <div>Aprobado</div>
                                        </div>
                                    @endif
                                </div>
                                <div class="xs">
                                    {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $item->created_at)->setTimezone('America/Bogota')->format("Y-m-d H:i") }}
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a target="_blank" href="{{ route('admin.orders.details', $item->id) }}" class="text-indigo-600 hover:text-indigo-900">Detalles</a>&nbsp;
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="p-5">
        {{ $items->links() }}
    </div>


@endsection
