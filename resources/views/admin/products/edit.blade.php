@extends('_layouts.admin')

@section('content')
    <div class="flex items-center justify-between mb-5">
        <h1 class="text-2xl font-semibold">Productos &raquo; Editar</h1>
    </div>


    <edit-product :product="{{ $product }}" token="{{ csrf_token() }}" :categories="{{ $categories }}" :ingredients="{{ $ingredients }}"></edit-product>
@endsection