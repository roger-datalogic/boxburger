@extends('_layouts.admin')

@section('content')
    <div class="flex items-center justify-between mb-5">
        <h1 class="text-2xl font-semibold">Productos &raquo; Agregar</h1>
    </div>


    <create-product token="{{ csrf_token() }}" :categories="{{ $categories }}" :ingredients="{{ $ingredients }}"></create-product>
@endsection