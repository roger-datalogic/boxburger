@extends('_layouts.admin')

@section('content')
    <div class="flex items-center justify-between mb-5">
        <h1 class="text-2xl font-semibold">Imagen Home</h1>
    </div>

    <form action="/admin/others" method="post" class="max-w-5xl" enctype="multipart/form-data">
        @csrf

        <div class="mb-4">
            <div class="mb-1">
                @if(file_exists(storage_path('app/public/pictures/home-custom-desktop.jpg')))
                    <img class="max-w-full w-1/2 h-auto" src="/storage/pictures/home-custom-desktop.jpg" alt="">
                @else
                    <img class="max-w-full w-1/2 h-auto" src="/images/bg-md.jpg" alt="">
                @endif
            </div>
            <label class="block text-sm font-medium text-gray-700">
                Imagen Home Escritorio (JPG 1800 x 720)
            </label>
            <div class="mt-1">
                <input type="file" name="picture">
            </div>
        </div>

        <div class="mb-4 mt-10">
            <div class="mb-1">
                @if(file_exists(storage_path('app/public/pictures/home-custom-mobile.jpg')))
                    <img class="max-w-full w-1/4 h-auto" src="/storage/pictures/home-custom-mobile.jpg" alt="">
                @else
                    <img class="max-w-full w-1/4 h-auto" src="/images/bg.jpg" alt="">
                @endif
            </div>
            <label class="block text-sm font-medium text-gray-700">
                Imagen Home Móvil (JPG 600 x 842)
            </label>
            <div class="mt-1">
                <input type="file" name="picture2">
            </div>
        </div>

        <div class="mt-4">
            <button type="submit" class="btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                </svg>

                <span>Actualizar</span>
            </button>
        </div>
    </form>


@endsection
