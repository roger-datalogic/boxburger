import Home from "./Home.vue";
import Foot from "./Foot.vue";

require("./bootstrap");

import { createApp } from "vue";

createApp({
    components: {
        Home,
        Foot,
    },
}).mount("#app");
