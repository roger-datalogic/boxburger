require('./bootstrap')

import { createApp } from "vue"
import CreateProduct from "./CreateProduct.vue"
import EditProduct from "./EditProduct.vue"

createApp({
    components: {
        CreateProduct,
        EditProduct
    }
}).mount('#app')
